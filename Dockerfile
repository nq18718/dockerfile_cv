FROM alpine

LABEL maintainer="Nuno Queiroz"

COPY . /opt/my_dockerfile

WORKDIR /opt/my_dockerfile

CMD ["cat","cv.txt"]
