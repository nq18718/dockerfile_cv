**Como proceder para visualizar o CV do grupo3 - by Nuno Queiroz**

---

## Fazer o git clone deste projecto:

_git clone git@bitbucket.org:nq18718/dockerfile\_cv.git_

---

## Entrar no directório criado:

_cd dockerfile\_cv/_

---

## Executar o seguinte comando:

_docker build -t my\_dockerfile:v1 ._

---

## Seguidamente por o container em andamento: 

_docker run my\_dockerfile:v1_


##FIM
